4x_MeguUp
-----
MeguUp is a model for [ESRGAN](https://github.com/xinntao/ESRGAN). Using [Joey's fork](https://github.com/JoeyBallentine/ESRGAN) is recommended.   
This project is being primarily made for [Game Upscale Discord](https://discord.gg/cpAUpDK) and [subreddit](https://www.reddit.com/r/GameUpscale/)  
[Dataset](https://yande.re/post?tags=game_cg+width%3A%3E%3D1920+ext%3Apng+-jpeg_artifacts+-possibly_upscaled%3F+limit%3A1000+) (NSFW warning) | Interpolated pretrained model (Currently unavailable.)